// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "TripleACharacter.h"
#include "IACharacter.h"
#include "BTTask_PlayerInVision.h"



EBTNodeResult::Type UBTTask_PlayerInVision::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8 * memory) {


	AAIController * controller = ownerComp.GetAIOwner();
	if (!controller)
		return EBTNodeResult::Failed;

	UBlackboardComponent * bbcomp = ownerComp.GetBlackboardComponent();
	if (!bbcomp)
		return EBTNodeResult::Failed;
	ATripleACharacter * player = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	AIACharacter* mychar = Cast<AIACharacter>(controller->GetPawn());
	bbcomp->SetValueAsObject("Player", player);
	bbcomp->SetValueAsBool("InVision", mychar->onVisionPlayer);
	return EBTNodeResult::Succeeded;
	
}



