// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "TripleACharacter.h"
#include "BaldosaActivadora.h"


// Sets default values
ABaldosaActivadora::ABaldosaActivadora()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = box;
	//box->SetupAttachment(mesh);

	box->OnComponentBeginOverlap.AddDynamic(this, &ABaldosaActivadora::OnOverlapBegin);
	box->OnComponentEndOverlap.AddDynamic(this, &ABaldosaActivadora::OnOverlapEnd);
	EndGamesucces = false;
	box->SetWorldScale3D(FVector(10, 10, 1));
}

// Called when the game starts or when spawned
void ABaldosaActivadora::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABaldosaActivadora::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaldosaActivadora::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp) {
		ATripleACharacter* player = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (player == OtherActor) {
			EndGamesucces = true;
			player->mygamePaused = true;
		}
	}

}

void ABaldosaActivadora::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		ATripleACharacter* player = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (player == OtherActor) {
			
		}

	}
}