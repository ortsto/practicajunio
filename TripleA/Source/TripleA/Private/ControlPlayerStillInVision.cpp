// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "IACharacter.h"
#include "ControlPlayerStillInVision.h"



EBTNodeResult::Type UControlPlayerStillInVision::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8 * memory) {


	AAIController * controller = ownerComp.GetAIOwner();
	if (!controller)
		return EBTNodeResult::Failed;

	UBlackboardComponent * bbcomp = ownerComp.GetBlackboardComponent();
	if (!bbcomp)
		return EBTNodeResult::Failed;

	AIACharacter* mychar = Cast<AIACharacter>(controller->GetPawn());

	if (mychar->onVisionPlayer == false) {
		auto timetoCount = UGameplayStatics::GetRealTimeSeconds(GetWorld());

		timetoCount = timetoCount - mychar->timeStart;
		if (timetoCount > 5.0f)
		{
			mychar->successTimeWithoutSee = true;
			bbcomp->SetValueAsBool("InVision", false);
			mychar->onVisionPlayer = false;
		}
	}
	return EBTNodeResult::Succeeded;

}
