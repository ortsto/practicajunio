// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "TripleACharacter.h"
#include "FinalDoor.h"


// Sets default values
AFinalDoor::AFinalDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	box->SetWorldScale3D(FVector(15, 10, 10));
	//RootComponent = box;
	box->SetupAttachment(RootComponent);
	box->OnComponentBeginOverlap.AddDynamic(this, &AFinalDoor::OnOverlapBegin);
	box->OnComponentEndOverlap.AddDynamic(this, &AFinalDoor::OnOverlapEnd);
	ComprobeAll = false;
}

// Called when the game starts or when spawned
void AFinalDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFinalDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFinalDoor::comprobeallKeys() {

}
void AFinalDoor::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp) {
		ATripleACharacter* player = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (player == OtherActor) {
			
		
			
			TArray<FString> key = {"Red","Green","Blue","Yellow"};
			for (int i = 0; i < key.Num(); i++) {
				ComprobeAll = player->searchKey(key[i]);
				if (ComprobeAll == 0) {
					break;
				}
			}
			/*
			ComprobeAll = player->searchKey("Red");
			ComprobeAll = player->searchKey("Yellow");
			ComprobeAll = player->searchKey("Green");
			ComprobeAll = player->searchKey("Blue");
			*/
		}
	}

}

void AFinalDoor::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{


	}
}