// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "AIController.h"
#include "IACharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "IACharacter.h"
#include "GetNextPoint.h"






EBTNodeResult::Type UGetNextPoint::ExecuteTask(UBehaviorTreeComponent& owenerComp, uint8 * NodeMemory) {

	AAIController * controller = owenerComp.GetAIOwner();
	if (!controller)
		return EBTNodeResult::Failed;

	UBlackboardComponent * bbcomp = owenerComp.GetBlackboardComponent();
	if (!bbcomp)
		return EBTNodeResult::Failed;

	AIACharacter* mychar = Cast<AIACharacter>(controller->GetPawn());
	bbcomp->SetValueAsObject("TargetPoint", mychar->GetNextPatrolPoint());
	return EBTNodeResult::Succeeded;
}