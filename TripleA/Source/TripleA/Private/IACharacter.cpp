// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "IACharacter.h"
#include "TripleACharacter.h"

// Sets default values
AIACharacter::AIACharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	currentPatrolPoint = -1;
	onVisionPlayer = false;
	startPositions[0] = FVector(-460, 620, 338);
	startPositions[1] = FVector(-460, 5250, 338);
	startPositions[2] = FVector(-460, 4120, 338);
	startPositions[3] = FVector(2289, -500, 338);
	//aipercetion = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AI Perception Component"));
	//SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AI Sight Config"));
	//aipercetion->OnPerceptionUpdated.AddDynamic(this, &ABaseCharacter::UpdatePerception);
	firstime = false;
}

AActor* AIACharacter::GetNextPatrolPoint() {

	//AActor * TmpTarget = patrolList[currentPatrolPoint];

	if (currentPatrolPoint >= patrolList.Num() - 1) {

		currentPatrolPoint = 0;
	}
	else {
		currentPatrolPoint++;
	}


	return patrolList[currentPatrolPoint];

}


// Called when the game starts or when spawned
void AIACharacter::BeginPlay()
{
	Super::BeginPlay();
	int random = FMath::RandRange(0, startNodes.Num()-1);
	//this->SetActorLocation(startPositions[random]);
	
	//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "...");
	this->SetActorLocation(startNodes[random]->GetActorLocation());
}

// Called every frame
void AIACharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	/*if(onVisionPlayer==true)
	{
		firstime = true;
	}else
	{
		if (firstime == true) {
			timeStart = UGameplayStatics::GetRealTimeSeconds(GetWorld());
			firstime = false;
		}
	}*/
}

// Called to bind functionality to input
void AIACharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void AIACharacter::ComprobeHIT(AActor* OtherActor, UPrimitiveComponent* OtherComp)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		ATripleACharacter *character = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (OtherActor == character) {
			character->SetPlayerDead(true);
		}
	}
}

