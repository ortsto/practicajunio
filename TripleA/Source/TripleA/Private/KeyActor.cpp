// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "TripleACharacter.h"
#include "IACharacter.h"
#include "KeyActor.h"


// Sets default values
AKeyActor::AKeyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	
	box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = mesh;
	box->SetupAttachment(mesh);
	box->OnComponentBeginOverlap.AddDynamic(this, &AKeyActor::OnOverlapBegin);
	box->OnComponentEndOverlap.AddDynamic(this, &AKeyActor::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AKeyActor::BeginPlay()
{
	Super::BeginPlay();
	switch (sayColor)
	{
	case vColorKey::kColorKey_Red:colorKeyString_ = "Red";
		break;
	case vColorKey::kColorKey_Yellow:colorKeyString_ = "Yellow";
		break;
	case vColorKey::kColorKey_Green:colorKeyString_ = "Green";
		break;
	case vColorKey::kColorKey_Blue:colorKeyString_ = "Blue";
		break;
	default: colorKeyString_ = "Red";
		break;
	}
}

// Called every frame
void AKeyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKeyActor::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp){
	ATripleACharacter* player = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (player == OtherActor) {
			player->insertKey(colorKeyString_);
			this->Destroy();
		}
	}

}

void AKeyActor::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{


	}
}