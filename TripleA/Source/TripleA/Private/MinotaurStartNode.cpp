// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "MinotaurStartNode.h"


// Sets default values
AMinotaurStartNode::AMinotaurStartNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMinotaurStartNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMinotaurStartNode::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

