// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "BehaviorTree/BlackboardComponent.h" 
#include "BehaviorTree/BehaviorTree.h"
#include "IACharacter.h"
#include "TripleAIAController.h"




ATripleAIAController::ATripleAIAController() {

	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BBComp"));
	BehaviorTreeComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BTComp"));

}

void ATripleAIAController::Possess(APawn* InPawn) {

	Super::Possess(InPawn);

	AIACharacter * MyChar = Cast<AIACharacter>(InPawn);

	if (MyChar) {
		BlackboardComp->InitializeBlackboard(*(MyChar->MyBehavior->BlackboardAsset));
		BehaviorTreeComp->StartTree(*(MyChar->MyBehavior));
	}

}
