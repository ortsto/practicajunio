// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "TripleACharacter.h"
#include "activatorActor.h"


// Sets default values
AactivatorActor::AactivatorActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = mesh;
	box->SetupAttachment(mesh);

	box->OnComponentBeginOverlap.AddDynamic(this, &AactivatorActor::OnOverlapBegin);
	box->OnComponentEndOverlap.AddDynamic(this, &AactivatorActor::OnOverlapEnd);
	touchedByPlayer = false;
}

// Called when the game starts or when spawned
void AactivatorActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AactivatorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AactivatorActor::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp) {
		ATripleACharacter* player = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (player == OtherActor) {
			touchedByPlayer = true;
		}
	}

}

void AactivatorActor::OnOverlapEnd_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		ATripleACharacter* player = Cast<ATripleACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (player == OtherActor) {
			touchedByPlayer = false;
		}

	}
}