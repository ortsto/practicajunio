// Fill out your copyright notice in the Description page of Project Settings.

#include "TripleA.h"
#include "movableMeshes.h"


// Sets default values
AmovableMeshes::AmovableMeshes()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	stateDone = false;
}

// Called when the game starts or when spawned
void AmovableMeshes::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AmovableMeshes::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AmovableMeshes::callbackhiddenParts() {
	GetWorldTimerManager().SetTimer(hiddenTimer, this, &AmovableMeshes::hiddenParts, 1.0f, false);
	stateDone = true;
}

void AmovableMeshes::callbackunhiddenParts() {

}

void AmovableMeshes::hiddenParts() {
	for (int i = 0; i < startedHidden.Num(); i++) {
		startedHidden[i]->SetActorHiddenInGame(true);
		startedHidden[i]->SetActorEnableCollision(false);
	}
	GetWorldTimerManager().SetTimer(unhiddenTimer, this, &AmovableMeshes::unhiddenParts, timeHidden, false);
	
}
void AmovableMeshes::unhiddenParts() {
	for (int i = 0; i < startedHidden.Num(); i++) {
		startedHidden[i]->SetActorHiddenInGame(false);
		startedHidden[i]->SetActorEnableCollision(true);
	}
	stateDone = false;
}

bool AmovableMeshes::getStateDone()
{
	return stateDone;
}
