// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "ControlPlayerStillInVision.generated.h"

/**
 * 
 */
UCLASS()
class TRIPLEA_API UControlPlayerStillInVision : public UBTTaskNode
{
	GENERATED_BODY()
	
public :
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8* memory) override;
	
	
};
