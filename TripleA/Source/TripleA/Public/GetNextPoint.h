// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "GetNextPoint.generated.h"

/**
 * 
 */
UCLASS()
class TRIPLEA_API UGetNextPoint : public UBTTaskNode
{
	GENERATED_BODY()
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp,
		uint8 * NodeMemory) override;
	
	
	
};
