// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "BehaviorTree/BehaviorTree.h"
#include "IACharacter.generated.h"

UCLASS()
class TRIPLEA_API AIACharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AIACharacter();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		TArray<AActor*> patrolList;
	UFUNCTION()
		AActor* GetNextPatrolPoint();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		UBehaviorTree * MyBehavior;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		bool onVisionPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		TArray<AActor*> startNodes;
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		class UAIPerceptionComponent * aipercetion;
	UPROPERTY(BlueprintReadWrite, Category = Senses)
		class UAISenseConfig_Sight* SightConfig;*/
	FVector startPositions[4];
	bool firstime;
	UFUNCTION(BlueprintCallable, Category = "CollisionEvent")
		void ComprobeHIT(AActor* OtherActor, UPrimitiveComponent* OtherComp);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Timer see")
		float timeStart;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bool Time")
		bool successTimeWithoutSee;
private:
	UPROPERTY()
		int currentPatrolPoint;

	
};
