// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "KeyActor.generated.h"


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class vColorKey : uint8
{
	kColorKey_Red = 0,
	kColorKey_Yellow,
	kColorKey_Green,
	kColorKey_Blue,
};

UCLASS()
class TRIPLEA_API AKeyActor : public AActor
{
	GENERATED_BODY()
public:	


	// Sets default values for this actor's properties
	AKeyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collision Components")
	class UBoxComponent * box;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh Components")
	class UStaticMeshComponent *	mesh;
	UFUNCTION(BlueprintNativeEvent, Category = "Collision Functions")
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void OnOverlapBegin_Implementation(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintNativeEvent, Category = "Collision Functions")
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	void OnOverlapEnd_Implementation(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables Keys")
	vColorKey sayColor;
	FString colorKeyString_;
};
