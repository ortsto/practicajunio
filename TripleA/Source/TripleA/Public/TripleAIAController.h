// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "TripleAIAController.generated.h"

/**
 * 
 */
UCLASS()
class TRIPLEA_API ATripleAIAController : public AAIController
{
	GENERATED_BODY()
public:
	ATripleAIAController();
	virtual void Possess(APawn* InPawn) override;

	UPROPERTY(EditAnywhere, Category = Behaviour)
		UBlackboardComponent* BlackboardComp;

	UPROPERTY(EditAnywhere, Category = Behaviour)
		UBehaviorTreeComponent* BehaviorTreeComp;

	
	
};
