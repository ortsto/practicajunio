// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "movableMeshes.generated.h"

UCLASS()
class TRIPLEA_API AmovableMeshes : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AmovableMeshes();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actores")
		TArray<AActor*> startedHidden;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actores")
		TArray<AActor*> starteddUnhidden;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	UFUNCTION(BlueprintCallable, Category = "Start timer for hide")
	void callbackhiddenParts();
	UFUNCTION(BlueprintCallable, Category = "State movable mesh")
		bool getStateDone();
	void callbackunhiddenParts();
	void hiddenParts();
	void unhiddenParts();
	virtual void Tick(float DeltaTime) override;
	FTimerHandle hiddenTimer;
	FTimerHandle unhiddenTimer;
	float resetTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision Done")
		bool stateDone;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables Keys")
		float timeHidden;
	
};
