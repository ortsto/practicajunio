// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "TripleA.h"
#include "Engine.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "TripleACharacter.h"

//////////////////////////////////////////////////////////////////////////
// ATripleACharacter

ATripleACharacter::ATripleACharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
	time_live = 0.0f;
	PrimaryActorTick.bCanEverTick = true;
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	mygamePaused = false;
	isDead = false;
	CrouchButtonDown = false;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATripleACharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Pause", IE_Released, this, &ATripleACharacter::PausetoMenu);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ATripleACharacter::crouchpressed);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ATripleACharacter::crouchreleased);
	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &ATripleACharacter::runningpressed);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &ATripleACharacter::runningreleased);
	PlayerInputComponent->BindAxis("MoveForward", this, &ATripleACharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATripleACharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATripleACharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATripleACharacter::LookUpAtRate);
	
	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ATripleACharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ATripleACharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ATripleACharacter::OnResetVR);
}


void ATripleACharacter::PausetoMenu()
{
	mygamePaused = !mygamePaused;
	//UGameplayStatics::SetGamePaused(GetWorld(),mygamePaused);
}


void ATripleACharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATripleACharacter::crouchpressed()
{
	CrouchButtonDown = true;
}

void ATripleACharacter::crouchreleased()
{
	CrouchButtonDown = false;
}

void ATripleACharacter::runningpressed()
{
	canRun = true;
}

void ATripleACharacter::runningreleased()
{
	canRun = false;
}



void ATripleACharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void ATripleACharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void ATripleACharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATripleACharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATripleACharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ATripleACharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}


bool ATripleACharacter::CheckIsDead() {
	if (isDead == true) {
		return true;
	}
	else {
		return false;
	}
}

void ATripleACharacter::SetPlayerDead(bool ali) {
	isDead = ali;
}

void ATripleACharacter::insertKey(FString key) {
	keysList_.Add(key);
}

bool ATripleACharacter::searchKey(FString find) {
	int32 value = keysList_.Find(find);
	GEngine->AddOnScreenDebugMessage(0, 2.f, FColor::Yellow,  find+" "+FString::FromInt(value));
	if (value >= 0) {
		return true;
	}
	else {
		return false;
	}
}

float ATripleACharacter::timeSpended() {
	if (mygamePaused == false && isDead ==false) {
		time_live = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	}
	else {
		time_live = time_live;
	}
	return time_live;

}


void ATripleACharacter::DoCameraFade() {
	APlayerCameraManager* playercm = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
	playercm->StartCameraFade(0, 1, 3.0f, FLinearColor(0, 0, 0), false, true);
	
}