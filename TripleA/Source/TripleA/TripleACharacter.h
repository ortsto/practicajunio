// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "TripleACharacter.generated.h"

UCLASS(config=Game)
class ATripleACharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ATripleACharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		bool isDead;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
		bool mygamePaused;
	UFUNCTION(BlueprintCallable, Category = "CharacterALive")
		bool CheckIsDead();
	UFUNCTION(BlueprintCallable, Category = "CharacterALive")
		void SetPlayerDead(bool ali);
	UFUNCTION(BlueprintCallable, Category = "InvetoryKey")
	void insertKey(FString a);
	UFUNCTION(BlueprintCallable, Category = "InvetoryKey")
	bool searchKey(FString find);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = KeysGame)
	TArray<FString> keysList_;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = RunBabyRun)
		bool canRun;
	UFUNCTION(BlueprintCallable, Category = "CameraFade")
		void DoCameraFade();
	UFUNCTION(BlueprintCallable, Category = "TimeLive")
		float timeSpended();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = TimeLivePlayer)
	float time_live;
protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void crouchpressed();
	void crouchreleased();
	void runningpressed();
	void runningreleased();
	void PausetoMenu();
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Crouch bool")
		bool CrouchButtonDown;
	
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

