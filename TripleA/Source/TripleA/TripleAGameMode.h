// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TripleAGameMode.generated.h"

UCLASS(minimalapi)
class ATripleAGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATripleAGameMode();
};



